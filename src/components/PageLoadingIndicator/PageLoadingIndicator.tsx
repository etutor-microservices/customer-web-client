import { Flex, Spinner } from '@chakra-ui/react';
import React, { ReactElement } from 'react';

export function PageLoadingIndicator(): ReactElement {
  return (
    <Flex h="calc(100vh - 133px)" align="center" justify="center">
      <Spinner size="xl" />
    </Flex>
  );
}
