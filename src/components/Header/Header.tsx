import {
  Box,
  Container,
  Flex,
  Heading,
  IconButton,
  useBreakpointValue,
  useColorModeValue,
  useDisclosure,
} from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { AiOutlineClose } from 'react-icons/ai';
import { FiMenu } from 'react-icons/fi';
import { NavLink } from 'react-router-dom';
import { LanguageSwitch } from '../../features/settings/components/LanguageSwitch';
import { ThemeSwitch } from '../../features/settings/components/ThemeSwitch';
import { AuthButtons } from '../AuthButtons';
import { Navigation } from '../Navigation';
import { SideMenu } from '../SideMenu';
import styles from './Header.module.css';

export function Header(): ReactElement {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const bgColor = useColorModeValue('gray.50', 'gray.700');
  const buttonSize = useBreakpointValue({ xl: 'lg', md: 'md' });
  const menuButtonDisplay = useBreakpointValue({ base: 'flex', xl: 'none' });
  const authDisplay = useBreakpointValue({ base: 'none', xl: 'flex' });

  return (
    <>
      <Container maxW="container.2xl" py={2}>
        <Flex justify="flex-end" gap={3}>
          <LanguageSwitch />
          <ThemeSwitch />
        </Flex>
      </Container>

      <Container
        maxW="container.2xl"
        bgColor={bgColor}
        className={styles.container}
        position="sticky"
      >
        <Flex direction="row" align="center" justify="space-between" gap={10}>
          <NavLink to="/">
            <Heading as="h1" size="lg" fontWeight={400} letterSpacing={-3}>
              E-TUTOR
            </Heading>
          </NavLink>

          <Navigation />

          <Box display={authDisplay}>
            <AuthButtons />
          </Box>

          <IconButton
            display={menuButtonDisplay}
            onClick={onOpen}
            aria-label="menu-icon-btn"
            icon={isOpen ? <AiOutlineClose /> : <FiMenu />}
            size={buttonSize}
          />
        </Flex>
      </Container>

      <SideMenu isOpen={isOpen} onClose={onClose} />
    </>
  );
}
