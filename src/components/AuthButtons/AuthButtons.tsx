import { Button, Flex, useBreakpointValue } from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

export function AuthButtons(): ReactElement {
  const { t } = useTranslation();

  const buttonSize = useBreakpointValue({ xl: 'lg', md: 'md' });

  return (
    <Flex align="center" gap={3} justify="space-between">
      <Button size={buttonSize} width="100%">
        {t('Login')}
      </Button>
      <Button colorScheme="blue" size={buttonSize} width="100%">
        {t('Register')}
      </Button>
    </Flex>
  );
}
