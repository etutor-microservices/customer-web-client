import { Flex, Heading } from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { IoIosCodeWorking } from 'react-icons/io';

export function InProgress(): ReactElement {
  const { t } = useTranslation();

  return (
    <Flex
      align="center"
      justify="center"
      direction="column"
      height="calc(100vh - 133px)"
    >
      <Heading fontWeight={300}>{t('In progress')}</Heading>
      <IoIosCodeWorking fontSize={60} />
    </Flex>
  );
}
