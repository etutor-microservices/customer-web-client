import { Box, Flex, useColorModeValue } from '@chakra-ui/react';
import React, { PropsWithChildren, ReactElement } from 'react';
import { Footer } from '../Footer';
import { Header } from '../Header';

export function Layout({ children }: PropsWithChildren<{}>): ReactElement {
  const contentBgColor = useColorModeValue('gray.200', 'gray.900');

  return (
    <Flex
      direction="column"
      minH="100vh"
      position="relative"
      bg={contentBgColor}
    >
      <Header />
      <Box flex={1}>{children}</Box>
      <Footer />
    </Flex>
  );
}
