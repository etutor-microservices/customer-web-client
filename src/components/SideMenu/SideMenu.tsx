import {
  Drawer,
  DrawerBody,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  Flex,
} from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';
import { navItems } from '../../constants';
import { AuthButtons } from '../AuthButtons';
import styles from './SideMenu.module.css';

interface SideMenuProps {
  isOpen: boolean;
  onClose: () => void;
}

export function SideMenu({ isOpen, onClose }: SideMenuProps): ReactElement {
  const { t } = useTranslation();

  return (
    <Drawer isOpen={isOpen} onClose={onClose} placement="left">
      <DrawerOverlay />
      <DrawerContent>
        <DrawerHeader>Menu</DrawerHeader>
        <DrawerBody>
          <Flex direction="column" justify="space-between" height="100%">
            <Flex direction="column" p={1} gap={3}>
              {React.Children.toArray(
                navItems.map(({ name, path }) => (
                  <NavLink
                    to={path}
                    onClick={onClose}
                    className={styles.linkContainer}
                  >
                    {t(name)}
                  </NavLink>
                ))
              )}
            </Flex>
            <AuthButtons />
          </Flex>
        </DrawerBody>
      </DrawerContent>
    </Drawer>
  );
}
