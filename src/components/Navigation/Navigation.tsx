import { Flex, Text, useBreakpointValue, useColorMode } from '@chakra-ui/react';
import React, { Children, ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';
import { navItems } from '../../constants';
import styles from './Navigation.module.css';

export function Navigation(): ReactElement {
  const { t } = useTranslation();
  const { colorMode } = useColorMode();
  const display = useBreakpointValue({ base: 'none', xl: 'flex' });

  const getActiveColor = () =>
    colorMode === 'light' ? 'black' : 'var(--white-color)';

  return (
    <Flex gap={7} display={display}>
      {Children.toArray(
        navItems.map(({ name, path }) => (
          <NavLink
            to={path}
            style={({ isActive }) => ({
              color: isActive ? getActiveColor() : 'var(--dark-color)',
            })}
          >
            <Text className={styles.link}>{t(name)}</Text>
          </NavLink>
        ))
      )}
    </Flex>
  );
}
