import React, { ReactElement } from 'react';
import { Helmet } from 'react-helmet';

interface PageWithTitleProps {
  title?: string;
}

export function Title({ title }: PageWithTitleProps): ReactElement {
  const renderTitleText = () => (title ? `${title} | ` : '');

  return (
    <Helmet>
      <title>{renderTitleText()}eTutor</title>
    </Helmet>
  );
}
