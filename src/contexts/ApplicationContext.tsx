import { createContext } from 'react';
import { TutorsService } from '../core';

interface ApplicationContextProps {
  tutorService: TutorsService;
}

export const ApplicationContext = createContext<
  Partial<ApplicationContextProps>
>({});
