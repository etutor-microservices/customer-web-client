export interface Degree {
  name: string;
  major: string;
  graduatedUniversity: string;
  dateOfIssue: string;
  academicRank: number;
}
