export interface Certificate {
  name: string;
  placeOfIssue: string;
  dateOfIssue: string;
  expiresIn: string;
}
