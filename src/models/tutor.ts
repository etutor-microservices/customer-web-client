export interface Tutor {
  id: string;
  firstName: string;
  middleName: string;
  lastName: string;
  dateOfBirth: string;
}
