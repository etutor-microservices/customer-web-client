import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

i18n.use(initReactI18next).init({
  lng: 'vn',
  fallbackLng: 'en',
  debug: true,

  resources: {
    en: {
      translation: {
        // Common
        Login: 'Login',
        Register: 'Register',
        January: 'January',
        February: 'February',
        March: 'March',
        April: 'April',
        May: 'May',
        June: 'June',
        July: 'July',
        August: 'August',
        September: 'September',
        October: 'October',
        November: 'November',
        December: 'December',

        // Header
        Home: 'Home',
        'Find tutors': 'Find tutors',
        Careers: 'Careers',
        'About us': 'About us',
        'Why us?': 'Why us?',

        // Not found
        'Page cannot be found': 'Page cannot be found',
        'Back to home': 'Back to home',

        // In progress
        'In progress': 'In progress',

        // Tutor details
        Female: 'Female',
        Male: 'Male',
        Gender: 'Gender',
        'Issues in': 'Issues in',
        'Expires in': 'Expires in',
        'Issued by': 'Issued by',
        Certificates: 'Certificates',
        Degrees: 'Degrees',
        Name: 'Name',
        Title: 'Title',
        University: 'University',
        Major: 'Major',
      },
    },
    vn: {
      translation: {
        // Common
        Login: 'Đăng nhập',
        Register: 'Đăng ký',
        January: 'Tháng Một',
        February: 'Tháng Hai',
        March: 'Tháng Ba',
        April: 'Tháng Tư',
        May: 'Tháng Năm',
        June: 'Tháng Sáu',
        July: 'Tháng Bảy',
        August: 'Tháng Tám',
        September: 'Tháng Chín',
        October: 'Tháng Mười',
        November: 'Tháng Mười Một',
        December: 'Tháng Mười Hai',

        // Header
        Home: 'Trang chủ',
        'Find tutors': 'Tìm kiếm gia sư',
        Careers: 'Cơ hội nghề nghiệp',
        'About us': 'Về chúng tôi',
        'Why us?': 'Lý do chọn e-tutor?',

        // Not found
        'Page cannot be found': 'Trang bạn tìm kiếm hiện không tồn tại',
        'Back to home': 'Trở về trang chủ',

        // In progress
        'In progress': 'Đang hoàn thiện',

        // Tutor details
        Female: 'Nữ',
        Male: 'Nam',
        Gender: 'Giới tính',
        'Issues in': 'Ngày cấp',
        'Expires in': 'Ngày hết hạn',
        'Issued by': 'Được cấp bởi',
        Certificates: 'Chứng chỉ',
        Degrees: 'Bằng cấp',
        Name: 'Tên',
        Title: 'Học vị',
        University: 'Trường',
        Major: 'Chuyên ngành',
      },
    },
  },
});

export default i18n;
