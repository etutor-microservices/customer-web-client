export const navItems = [
  {
    name: 'Home',
    path: '/',
  },
  {
    name: 'Find tutors',
    path: '/tutors',
  },
  {
    name: 'Careers',
    path: '/careers',
  },
  {
    name: 'About us',
    path: '/about',
  },
  {
    name: 'Why us?',
    path: '/why-us',
  },
];
