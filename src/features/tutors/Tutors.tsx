import React, { ReactElement } from 'react';
import { Route, Routes } from 'react-router-dom';
import { TutorDetailsManagement } from './pages/TutorDetails';
import { TutorListManagement } from './pages/TutorList';

export function Tutors(): ReactElement {
  return (
    <Routes>
      <Route path="" element={<TutorListManagement />} />
      <Route path=":id" element={<TutorDetailsManagement />} />
    </Routes>
  );
}
