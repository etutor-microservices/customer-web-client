import {
  Container,
  Flex,
  Heading,
  IconButton,
  useBreakpointValue,
} from '@chakra-ui/react';
import React, { ReactElement, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { IoMdArrowDropleft, IoMdArrowDropright } from 'react-icons/io';
import { Degree } from '../../../../models/degree';
import { DegreeCard } from '../DegreeCard';

interface DegreeGridProps {
  list: Degree[];
}

export function DegreeGrid({ list }: DegreeGridProps): ReactElement {
  const { t } = useTranslation();

  const [degreeIndex, setDegreeIndex] = useState(0);
  const [degree, setDegree] = useState(list[degreeIndex]);

  const buttonSize = useBreakpointValue({ base: 'sm', md: 'md', lg: 'lg' });

  const onIndexChange = (changedDegreeIndex: number) => {
    setDegree(list[changedDegreeIndex]);
    setDegreeIndex(changedDegreeIndex);
  };

  return (
    <Flex direction="column" align="center" gap={4}>
      <Heading fontWeight={700} color="gray.500">
        {t('Degrees')}
      </Heading>

      <Container maxW="container.lg">
        <Flex align="center" gap={5} width="100%">
          <IconButton
            aria-label="degree-prev-btn"
            icon={<IoMdArrowDropleft />}
            variant="solid"
            borderRadius="50%"
            size={buttonSize}
            colorScheme="facebook"
            onClick={() => onIndexChange(degreeIndex - 1)}
            disabled={degreeIndex === 0}
          />

          <DegreeCard degree={degree} />

          <IconButton
            aria-label="degree-next-btn"
            icon={<IoMdArrowDropright />}
            variant="solid"
            borderRadius="50%"
            size={buttonSize}
            colorScheme="facebook"
            onClick={() => onIndexChange(degreeIndex + 1)}
            disabled={degreeIndex === list.length - 1}
          />
        </Flex>
      </Container>
    </Flex>
  );
}
