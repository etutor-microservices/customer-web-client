import { Container, Flex } from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { TutorDetails } from '../../../../models';
import { CertificateGrid } from '../CertificateGrid';
import { DegreeGrid } from '../DegreeGrid';
import { TutorDetailsMetadata } from '../TutorDetailsMetadata';

interface TutorDetailsGridProps {
  details: TutorDetails;
}

export function TutorDetailsGrid({
  details,
}: TutorDetailsGridProps): ReactElement {
  const {
    firstName,
    middleName,
    lastName,
    gender,
    dateOfBirth,
    email,
    description,
    certificates,
    degrees,
  } = details;

  return (
    <Container maxW="container.2xl" py={5}>
      <Flex direction="column" gap={20}>
        <TutorDetailsMetadata
          firstName={firstName}
          middleName={middleName}
          lastName={lastName}
          gender={gender}
          description={description}
          dateOfBirth={dateOfBirth}
          email={email}
        />

        <DegreeGrid list={degrees} />
        <CertificateGrid list={certificates} />
      </Flex>
    </Container>
  );
}
