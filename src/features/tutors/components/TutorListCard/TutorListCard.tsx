import { Box, Heading, Text, useColorModeValue } from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { useNavigate } from 'react-router-dom';
import { Tutor } from '../../../../models';
import styles from './TutorListCard.module.css';

interface TutorCardProps {
  tutor: Tutor;
}

export function TutorListCard({ tutor }: TutorCardProps): ReactElement {
  const navigate = useNavigate();
  const { id, firstName, middleName, lastName } = tutor;

  const bgColor = useColorModeValue('gray.50', 'gray.700');

  const onViewDetailsClick = () => {
    navigate(`/tutors/${id}`);
  };

  return (
    <Box bgColor={bgColor} className={styles.container}>
      <Heading onClick={onViewDetailsClick} cursor="pointer">
        {firstName}
      </Heading>
      <Text>
        {middleName} {lastName}
      </Text>
    </Box>
  );
}
