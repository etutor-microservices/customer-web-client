import { Box, Flex, Heading, Image, Text } from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { TutorDetails } from '../../../../models';
import styles from './TutorDetailsMetadata.module.css';

export function TutorDetailsMetadata({
  firstName,
  middleName,
  lastName,
  gender,
  email,
  description,
}: Pick<
  TutorDetails,
  | 'firstName'
  | 'middleName'
  | 'lastName'
  | 'gender'
  | 'dateOfBirth'
  | 'email'
  | 'description'
>): ReactElement {
  const { t } = useTranslation();

  return (
    <Flex className={styles.container}>
      <Box flex={1.4}>
        <Image
          width="100%"
          src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.the74million.org%2Fwp-content%2Fuploads%2F2017%2F01%2F1438645758_3237.png&f=1&nofb=1"
        />
      </Box>

      <Flex direction="column" flex={0.6} justify="center" gap={2}>
        <Heading fontWeight={400}>
          {firstName} {middleName} {lastName}
        </Heading>

        <Text>
          {t('Gender')}: {t(gender)}
        </Text>

        <Flex align="center" gap={2}>
          <Text>Gmail: {email}</Text>
        </Flex>

        <Text fontSize="1.15rem">{description}</Text>
      </Flex>
    </Flex>
  );
}
