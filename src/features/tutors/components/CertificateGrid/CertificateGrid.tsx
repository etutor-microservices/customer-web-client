import { Container, Flex, Heading, Tab, TabList, Tabs } from '@chakra-ui/react';
import React, { ReactElement, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { GoPrimitiveDot } from 'react-icons/go';
import { Certificate } from '../../../../models/certificate';
import { CertificateCard } from '../CertificateCard';

interface CertificateGridProps {
  list: Certificate[];
}

export function CertificateGrid({ list }: CertificateGridProps): ReactElement {
  const { t } = useTranslation();

  const [tabIndex, setTabIndex] = useState(0);

  // TODO: adjust number of item in chunk when screen width change
  const [numberOfItemInChunk] = useState(3);

  const onTabChange = (clickedTabIndex: number) => {
    setTabIndex(clickedTabIndex);
  };

  return (
    <Flex direction="column" gap={4} align="center">
      <Heading fontWeight={700} color="gray.500">
        {t('Certificates')}
      </Heading>

      <Container maxW="container.xl">
        <Flex gap={2}>
          {React.Children.toArray(
            list
              .slice(
                tabIndex * numberOfItemInChunk,
                tabIndex * numberOfItemInChunk + numberOfItemInChunk
              )
              .map((certificate) => (
                <CertificateCard certificate={certificate} />
              ))
          )}
        </Flex>
      </Container>

      <Tabs
        variant="soft-rounded"
        colorScheme="blue"
        defaultIndex={tabIndex}
        onChange={onTabChange}
      >
        <TabList>
          {React.Children.toArray(
            [...new Array(Math.round(list.length / numberOfItemInChunk))].map(
              () => (
                <Tab>
                  <GoPrimitiveDot />
                </Tab>
              )
            )
          )}
        </TabList>
      </Tabs>
    </Flex>
  );
}
