import {
  Box,
  Divider,
  Flex,
  Heading,
  Text,
  Image,
  useColorModeValue,
} from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { Certificate } from '../../../../models';
import { getDayInNumber, getMonthInText, getYear } from '../../../../utils';
import styles from './CertificateCard.module.css';

interface CertificateCardProps {
  certificate: Certificate;
}

export function CertificateCard({
  certificate,
}: CertificateCardProps): ReactElement {
  const { t } = useTranslation();

  const { name, dateOfIssue, placeOfIssue, expiresIn } = certificate;

  const backgroundColor = useColorModeValue('blue.50', 'blue.900');

  return (
    <Flex
      className={styles.container}
      _hover={{ backgroundColor }}
      direction="column"
      gap={2}
    >
      <Flex justify="space-between">
        <Flex direction="column">
          <Text>{t('Issues in')}</Text>
          <Text fontWeight={500} fontSize="1.5rem">
            {getDayInNumber(dateOfIssue)}
          </Text>
          <Text fontWeight={200} fontSize="1.3rem">
            {t(getMonthInText(dateOfIssue))}
          </Text>
          <Text fontWeight={300} fontSize="1rem">
            {getYear(dateOfIssue)}
          </Text>
        </Flex>

        <Flex direction="column" align="flex-end">
          <Text>{t('Expires in')}</Text>
          <Text fontWeight={500} fontSize="1.5rem">
            {getDayInNumber(expiresIn)}
          </Text>
          <Text fontWeight={200} fontSize="1.3rem">
            {t(getMonthInText(expiresIn))}
          </Text>
          <Text fontWeight={300} fontSize="1rem">
            {getYear(expiresIn)}
          </Text>
        </Flex>
      </Flex>

      <Divider />

      <Flex justify="space-between" direction="column" height="100%" gap={5}>
        <Box>
          <Heading size="lg" fontWeight={400}>
            {name}
          </Heading>

          <Text fontWeight={300}>
            {t('Issued by')}: {placeOfIssue}
          </Text>
        </Box>

        <Image
          className={styles.img}
          borderRadius={20}
          src="https://blakelivelybrasil.com/wp-content/uploads/2020/09/certificate-template-cdr-free-download-certificate-template-cdr-free-download-scaled.jpg"
        />
      </Flex>
    </Flex>
  );
}
