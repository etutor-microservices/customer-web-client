import { Container, Grid } from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { Tutor } from '../../../../models';
import { TutorListCard } from '../TutorListCard';

interface TutorListGridProps {
  list: Tutor[];
}

export function TutorListGrid({ list }: TutorListGridProps): ReactElement {
  return (
    <Container maxW="container.xl" py={5}>
      <Grid templateColumns="repeat(4, 1fr)" gap={6}>
        {React.Children.toArray(
          list.map((tutor) => <TutorListCard tutor={tutor} />)
        )}
      </Grid>
    </Container>
  );
}
