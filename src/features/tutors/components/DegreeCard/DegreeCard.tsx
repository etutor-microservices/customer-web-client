import { Flex, Image, Text, useColorModeValue } from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { Degree } from '../../../../models';
import { getDayInNumber, getMonthInText, getYear } from '../../../../utils';
import styles from './DegreeCard.module.css';

interface DegreeCardProps {
  degree: Degree;
}

export function DegreeCard({ degree }: DegreeCardProps): ReactElement {
  const { t } = useTranslation();

  const { name, academicRank, dateOfIssue, graduatedUniversity, major } =
    degree;

  const bgColor = useColorModeValue('gray.50', 'gray.800');

  return (
    <Flex className={styles.container}>
      <Flex direction="column" bgColor={bgColor} p={5} borderRadius={10}>
        <Text className={styles.text}>
          <span>{t('Name')}:</span> {name}
        </Text>
        <Text className={styles.text}>
          <span>{t('Title')}:</span> {academicRank}
        </Text>
        <Text className={styles.text}>
          <span>{t('University')}:</span> {graduatedUniversity}
        </Text>
        <Text className={styles.text}>
          <span>{t('Major')}:</span> {major}
        </Text>
        <Text className={styles.text}>
          <span>{t('Issues in')}:</span>{' '}
          {`${getDayInNumber(dateOfIssue)} ${t(
            getMonthInText(dateOfIssue)
          )}, ${getYear(dateOfIssue)}`}
        </Text>
      </Flex>

      <Image
        className={styles.img}
        src="https://cdn.slidesharecdn.com/ss_thumbnails/0143e5d6-091f-4c6f-b7d3-44e0cad76d3d-160518115934-thumbnail-4.jpg?cb=1463572877"
      />
    </Flex>
  );
}
