import React, { ReactElement, useContext } from 'react';
import { useQuery } from 'react-query';
import { useParams } from 'react-router-dom';
import { PageLoadingIndicator } from '../../../../components/PageLoadingIndicator';
import { Title } from '../../../../components/Title';
import { ApplicationContext } from '../../../../contexts';
import { TutorDetailsGrid } from '../../components/TutorDetailsGrid';

export function TutorDetailsManagement(): ReactElement {
  const { id } = useParams<{ id: string }>();
  const { tutorService } = useContext(ApplicationContext);

  const { data, isLoading } = useQuery(['tutors', id], () =>
    tutorService!.getDetails(id)
  );

  return (
    <>
      <Title title={`${data?.firstName} ${data?.lastName}`} />
      {isLoading ? (
        <PageLoadingIndicator />
      ) : (
        <TutorDetailsGrid details={data!} />
      )}
    </>
  );
}
