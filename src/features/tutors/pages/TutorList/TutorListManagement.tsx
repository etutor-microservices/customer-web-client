import React, { ReactElement, useContext, useEffect, useState } from 'react';
import { useQuery } from 'react-query';
import { Title } from '../../../../components/Title';
import { ApplicationContext } from '../../../../contexts';
import { Tutor } from '../../../../models';
import { TutorListGrid } from '../../components/TutorListGrid';

export function TutorListManagement(): ReactElement {
  const { tutorService } = useContext(ApplicationContext);

  const [tutorList, setTutorList] = useState<Tutor[]>([]);

  const { data } = useQuery('tutors', () => tutorService?.getList(1, 10));

  useEffect(() => {
    if (!data) return;
    setTutorList(data);
  }, [data]);

  return (
    <>
      <Title title="Tutors" />
      <TutorListGrid list={tutorList} />
    </>
  );
}
