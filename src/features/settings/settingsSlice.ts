import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

interface SettingsState {
  language: string;
}

const initialState: SettingsState = {
  language: '',
};

const slice = createSlice({
  name: 'settings',
  initialState,
  reducers: {
    setLanguage: (state, action: PayloadAction<string>) => {
      localStorage.setItem('language', action.payload);
      state.language = action.payload;
    },
  },
});

export const settingsAction = slice.actions;

const settingsReducer = slice.reducer;
export default settingsReducer;

export const settingsLanguageSelector = (state: RootState) =>
  state.settings.language;
