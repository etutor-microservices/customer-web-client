import { IconButton, useColorMode } from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { BsFillMoonStarsFill } from 'react-icons/bs';
import { FaSun } from 'react-icons/fa';

export function ThemeSwitch(): ReactElement {
  const { colorMode, toggleColorMode } = useColorMode();

  return (
    <IconButton
      aria-label="theme-switch"
      variant="ghost"
      onClick={toggleColorMode}
      size="sm"
      icon={colorMode === 'light' ? <BsFillMoonStarsFill /> : <FaSun />}
    />
  );
}
