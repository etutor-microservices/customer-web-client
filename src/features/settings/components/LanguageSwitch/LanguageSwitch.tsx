import { Button } from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { settingsAction, settingsLanguageSelector } from '../../settingsSlice';

export function LanguageSwitch(): ReactElement {
  const dispatch = useDispatch();
  const language = useSelector(settingsLanguageSelector);

  const onLangSwitchClick = () => {
    const changedLanguage = language === 'vn' ? 'en' : 'vn';
    dispatch(settingsAction.setLanguage(changedLanguage));
  };

  return (
    <Button variant="ghost" onClick={onLangSwitchClick} size="sm">
      {language === 'vn' ? 'EN' : 'VN'}
    </Button>
  );
}
