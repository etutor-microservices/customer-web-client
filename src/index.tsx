import React from 'react';
import ReactDOM from 'react-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import { store } from './app/store';
import { ApplicationContext } from './contexts';
import { BaseHttpClient, TutorsService } from './core';
import './i18n';
import './index.css';
import * as serviceWorker from './serviceWorker';

const queryClient = new QueryClient();

const baseHttpClient = new BaseHttpClient();

const services = {
  tutorService: new TutorsService(baseHttpClient),
};

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <QueryClientProvider client={queryClient}>
        <ApplicationContext.Provider value={services}>
          <App />
        </ApplicationContext.Provider>
      </QueryClientProvider>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
