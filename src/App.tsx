import { ChakraProvider, CSSReset } from '@chakra-ui/react';
import React, { Suspense, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Routes } from 'react-router-dom';
import { Layout } from './components/Layout';
import { PageLoadingIndicator } from './components/PageLoadingIndicator';
import {
  settingsAction,
  settingsLanguageSelector,
} from './features/settings/settingsSlice';
import { Home } from './pages/Home';
import { NotFound } from './pages/NotFound';
import { theme } from './theme';

const Tutors = React.lazy(() =>
  import('./features/tutors').then(({ Tutors }) => ({ default: Tutors }))
);

const WhyUs = React.lazy(() =>
  import('./pages/WhyUs').then(({ WhyUs }) => ({ default: WhyUs }))
);

function App() {
  const { i18n } = useTranslation();

  const language = useSelector(settingsLanguageSelector);
  const dispatch = useDispatch();

  useEffect(() => {
    const storedLanguage = localStorage.getItem('language');
    if (!storedLanguage) {
      dispatch(settingsAction.setLanguage('en'));
      return;
    }
    dispatch(settingsAction.setLanguage(storedLanguage));
  }, [dispatch]);

  useEffect(() => {
    i18n.changeLanguage(language);
  }, [language, i18n]);

  return (
    <ChakraProvider theme={theme}>
      <CSSReset />
      <Layout>
        <Routes>
          <Route path="" element={<Home />} />

          <Route
            path="/tutors/*"
            element={
              <Suspense fallback={<PageLoadingIndicator />}>
                <Tutors />
              </Suspense>
            }
          />

          <Route
            path="/why-us"
            element={
              <Suspense fallback={<PageLoadingIndicator />}>
                <WhyUs />
              </Suspense>
            }
          />

          <Route path="*" element={<NotFound />} />
        </Routes>
      </Layout>
    </ChakraProvider>
  );
}

export default App;
