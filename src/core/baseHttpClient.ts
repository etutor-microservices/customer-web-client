import axios, { Axios, AxiosRequestConfig } from 'axios';
import qs from 'qs';
import { environment } from '../environment';

export class BaseHttpClient {
  private readonly _httpClient: Axios;

  constructor() {
    this._httpClient = axios.create({
      responseType: 'json',
      timeout: 15000,
      baseURL: environment.API_BASE_URL,
      paramsSerializer: (params) =>
        qs.stringify(params, { arrayFormat: 'comma' }),
      headers: {
        'content-type': 'application/json',
      },
    });
  }

  async callAPI<T>(config: AxiosRequestConfig) {
    const resp = await this._httpClient.request<T>(config);
    return resp.data;
  }
}
