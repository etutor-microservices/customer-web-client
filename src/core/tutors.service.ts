import { Tutor, TutorDetails } from '../models';
import { BaseHttpClient } from './baseHttpClient';

export class TutorsService {
  constructor(private readonly _httpClient: BaseHttpClient) {}

  getList(pageNumber: number, pageSize: number) {
    return this._httpClient.callAPI<Tutor[]>({
      url: `tutors?pageNumber=${pageNumber}&pageSize=${pageSize}`,
      method: 'GET',
    });
  }

  getDetails(id?: string) {
    if (!id) return;
    return this._httpClient.callAPI<TutorDetails>({
      url: `tutors/${id}`,
      method: 'GET',
    });
  }
}
