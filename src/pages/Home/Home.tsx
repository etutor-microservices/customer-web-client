import { Container } from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { InProgress } from '../../components/InProgress';
import { Title } from '../../components/Title';

export function Home(): ReactElement {
  return (
    <>
      <Title />
      <Container maxW="container.2xl">
        <InProgress />
      </Container>
    </>
  );
}
