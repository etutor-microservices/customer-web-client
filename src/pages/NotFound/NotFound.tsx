import { Flex, Heading, Text } from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import { NavLink } from 'react-router-dom';
import { Title } from '../../components/Title';
import { AiFillHome } from 'react-icons/ai';
import { useTranslation } from 'react-i18next';

export function NotFound(): ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <Title title="Not found" />
      <Flex
        align="center"
        justify="center"
        direction="column"
        gap={5}
        height="calc(100vh - 133px)"
      >
        <Heading fontWeight={100}>{t('Page cannot be found')}</Heading>
        <Heading fontSize="10rem" fontWeight={100}>
          404
        </Heading>
        <NavLink to="/">
          <Flex align="center" gap={1}>
            <AiFillHome />
            <Text>{t('Back to home')}</Text>
          </Flex>
        </NavLink>
      </Flex>
    </>
  );
}
