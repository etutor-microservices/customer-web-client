import {
  Container,
  Flex,
  Heading,
  Image,
  Text,
  useColorModeValue,
} from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import banner1 from '../../assets/WhyUs/banner1.png';
import banner2 from '../../assets/WhyUs/banner2.png';
import banner3 from '../../assets/WhyUs/banner3.png';
import { Title } from '../../components/Title';

const reasons = [
  {
    bigText: 'Đội ngũ chuyên nghiệp',
    smallText: 'Đội ngũ được tuyển dụng kĩ càng',
    image: banner1,
  },
  {
    bigText: 'Không lo dịch bệnh',
    smallText: 'An toàn tuyệt đối với phương pháp học trực tuyến',
    image: banner2,
  },
  {
    bigText: 'Tận tình dạy bảo',
    smallText: 'Đội ngũ gia sư tâm huyết dạy bảo học viên',
    image: banner3,
  },
];

export function WhyUs(): ReactElement {
  const textColor = useColorModeValue('gray.600', 'gray.300');

  return (
    <>
      <Title title="Why us?" />
      <Container maxW="container.xl" py={5}>
        <Flex direction="column" gap={10}>
          {React.Children.toArray(
            reasons.map(({ bigText, smallText, image }, index) => (
              <Flex
                justify="space-between"
                align="center"
                direction={index % 2 === 0 ? 'row' : 'row-reverse'}
              >
                <Flex direction="column">
                  <Heading>{bigText}</Heading>
                  <Text color={textColor}>{smallText}</Text>
                </Flex>
                <Image src={image} width="40%" />
              </Flex>
            ))
          )}
        </Flex>
      </Container>
    </>
  );
}
